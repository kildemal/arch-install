#!/bin/bash

function main(){

	#bool
	enablefingerprint=true
	removepartitions=true
	createpartitions=true


	#locale
	language="en_GB.UTF-8"
	timezone="Asia/Kolkata"


	#packages
	linuxpkg="base linux linux-firmware"
	develpkg="base-devel nano vim vi wget git go"
	shellpkg="fish"
	baseutilspkg="sudo man bash-completion $shellpkg reflector htop tmux tlp ffmpeg efibootmgr neofetch"

	microcodepkg="intel-ucode"
	filesystempkg="btrfs-progs"

	networkpkg="iwd dhcpcd"
	soundfirmware="sof-firmware"
	audiopkg="alsa-utils pipewire pipewire-pulse"
	bluetoothpkg="bluez bluez-utils"

	graphicsprovider="Intel"
	graphicsdriver="intel"
	graphicspkg="mesa xf86-video-$graphicsdriver vulkan-$graphicsdriver lib32-mesa lib32-vulkan-$graphicsdriver"

	xorgpkg="xorg-server xorg-xinit"
	xorgutils="xorg-xinput"
	iopkg="xf86-input-synaptics"

	fontspkg="cantarell-fonts noto-fonts noto-fonts-cjk noto-fonts-emoji ttf-indic-otf powerline-fonts"
	fontsaurpkg="nerd-fonts-complete"

	windowmanager="leftwm"
	thememanager="leftwm-theme"

	gtkthememanager="lxappearance"
	gtktheme="plata-theme"
	icontheme="tela-icon-theme"

	bar="polybar"
	menubar="dmenu-rs"

	lockscreen="i3lock-fancy-multimonitor"

	autosuspendscript="xidlehook"

	wallpaperagent="feh"
	keyboardshortcuts="sxhkd"
	compositor="picom-jonaburg-git"

	appinstaller="flatpak"
	utils="brightnessctl conky mupdf mupdf-tools pcmanfm python-pywal rofi"
	terminal="alacritty xterm"

	if [[ $enablefingerprint = true ]];
	then
		fingerprint="fprintd pam libusb imagemagick"
	else
		fingerprint=""
	fi
	

	aurwrapper="paru"

	pacmanpkg="$linuxpkg $microcodepkg $develpkg $baseutilspkg $filesystempkg $networkpkg $soundfirmware $audiopkg $bluetoothpkg $xorgpkg $graphicspkg $xorgutils $iopkg $fontspkg $wallpaperagent $keyboardshortcuts $appinstaller $utils $terminal $fingerprint $gtkthememanager"
	aurpkg="$fontsaurpkg $windowmanager $thememanager $bar $menubar $compositor $lockscreen $autosuspendscript $gtktheme $icontheme"
	

	#partitions
	installon="/dev/nvme0n1"
	ssdpartitionseperator="p"
	esppartition=1
	swappartition=2
	rootpartition=3


	#partition sizes
	espmib=512
	swapmib=4096
	rootmib=483775

	#partition location
	espstartmib=1
	espendmib=$((espstartmib + espmib))

	swapstartmib=$((espendmib))
	swapendmib=$((swapstartmib + swapmib))

	rootstartmib=$((swapendmib))
	rootendmib=$((rootstartmib + rootmib))

	#login
	pcusername="aim"
	pcpassword="aim"
	pchostname="localhost"

	#boot
	kernelmodules=""
	kerneldef="root=LABEL=Computer rw rootflags=subvol=@"
	kernelargs="snd_intel_dspcfg dsp_driver=1"
	
	#continue variable
	continuestate=1

	#try
	(
		set -e
		if [[ -z "$@" ]]
		then
			continueinstall
		else
			if [[ -z "$2" && -f "archinstalllog.txt" ]]
			then
				source "archinstalllog.txt"
				continuestate="$crashedstate"
			else
				continuestate="$2"
			fi
			export setupoption="$1"
			setup "$0"
		fi
	)

	# catch
	errorcode="$?"
	if [[ $errorcode -ne 0 ]]
	then
		echo "ERROR!!! Arch Linux Installer Failed at stage $continuestate!!"
		echo crashedstate="$continuestate" > archinstalllog.txt
		exit $continuestate
	fi
}


function setup(){
	case "$setupoption" in
		"-h" | "--help")
			echo ''
			echo 'Usage:'
			echo " $1"
			echo " $1 [options]"
			echo " $1 -c <continuecode>"
			echo ''
			echo 'Arch Linux Installer'
			echo ''
			echo 'Options:'
			echo ' -h,  --help           help'
			echo ' -u,  --update         update the install script'
			echo ' -c,  --continue       continue'
			echo ' -m,  --mount          mountall'
			echo ' -um, --umount         unmountall'
			echo ' -cr, --chroot         chroot into installed system'
			echo ' -ft, --fixtime        update system time              (1)'
			echo ' -p,  --part           partition disks                 (2)'
			echo ' -f,  --format         format disks                    (3)'
			echo ' -s,  --swap           turn on swap                    (4)'
			echo ' -r,  --root           setup root                      (5)'
			echo ' -m,  --mirror         refresh mirrors                 (6)'
			echo ' -i,  --install        install arch                    (7)'
			echo ' -et, --ntp            enable ntp                      (8)'
			echo ' -l,  --locale         generate locale                 (9)'
			echo ' -hn, --hostname       configure hostname              (10)'
			echo ' -in, --initramfs      generate initramfs              (11)'
			echo ' -cu, --createuser     create new user                 (12)'
			echo ' -eb, --bluetooth      enable bluetooth                (13)'
			echo ' -en, --network        enable dhcp and iwd             (14)'
			echo ' -et, --tlp            enable tlp                      (15)'
			echo ' -g,  --graphics       configure graphics              (16)'
			echo ' -fp, --fingerprint    enable fingerprint in pam       (17)'
			echo ' -tp, --setuptouchpad  setup touchpad config           (18)'
			echo ' -a,  --aur            install aur helpers             (19)'
			echo ' -ap, --aurpackages    install aur packages            (20)'
			echo ' -lt, --leftwmtheme    install leftwm theme            (21)'
			echo ' -gt, --gtktheme       install gtk theme and icons     (22)'
			echo ' -ls, --lockscreen     setup lockscreen for leftwm     (23)'
			echo ' -pc, --picom          setup picom blur effects        (24)'
			echo ' -sx, --sxhkd          setup sxhkd for media keys      (25)'
			echo ' -xi, --xinitrc        setup xinitrc for leftwm, sxhkd (26)'
			echo ' -fx, --startx         setup startx at login for fish  (27)'
			echo ' -b,  --boot           install bootloader              (28)'
			echo ' -sb, --secureboot     enable secureboot               (29)'
			echo ' -ds, --defaultshell   change default shell            (30)'
			echo ' -q,  --quit           quit installer                  (31)'
			echo ''
			echo 'Continue codes are given in brackets. Installer will proceed to execute it and all the instructions below it.'
			;;
		"-u" | "--update") updatescript "$1"
			;;
		"-c" | "--continue") continueinstall
			;;
		"-m" | "--mount") mountall
			;;
		"-um" | "--umount") unmountall
			;;
		"-cr" | "--chroot") archchroot
			;;
		"-ft" | "--fixtime") updatesystime
			;;
		"-p" | "--part") partitiondisks
			;;
		"-f" | "--format") formatdisks
			;;
		"-s" | "--swap") turnonswap
			;;
		"-r" | "--root") setuproot
			;;
		"-m" | "--mirror") refreshmirrors || true
			;;
		"-i" | "--install") installarchlinux
			;;
		"-et" | "--ntp") ntpsynctime
			;;
		"-l" | "--locale") genlocales
			;;
		"-hn" | "--hostname") sethostname
			;;
		"-in" | "--initramfs") geninitramfs
			;;
		"-cu" | "--createuser") createnewuser
			;;
		"-eb" | "--bluetooth") sdenablebluetooth
			;;
		"-en" | "--network") sdenablenetwork
			;;
		"-et" | "--tlp") sdenabletlp
			;;
		"-g" | "--graphics") setupgraphics
			;;
		"-fp" | "--fingerprint") setupfingerprint
			;;
		"-tp" | "--touchpad") setuptouchpad
			;;
		"-a" | "--aur") installaurhelpers
			;;
		"-ap" | "--aurpackages") installaurpackages
			;;
		"-lt" | "--leftwmtheme") installleftwmtheme
			;;
		"-gt" | "--gtktheme") setupgtktheme
			;;
		"-ls" | "--lockscreen") setuplockscreen
			;;
		"-pc" | "--picom") setuppicomconf
			;;
		"-sx" | "--sxhkd") setupsxhkdrc
			;;
		"-xi" | "--xinitrc") setupxinitrc
			;;
		"-fx" | "--startx") setupfishstartx
			;;
		"-b" | "--boot") installbootloader
			;;
		"-sb" | "--secureboot") installsecureboot
			;;
		"-ds" | "--defaultshell") changedefaultshell
			;;
		"-q" | "--quit") finishinstall
	esac
}

function continueinstall(){
	case "$continuestate" in
		1)  updatesystime
			continuestate=1
			;&
		2)  if [[ $createpartitions = true ]];
			then
				partitiondisks
			fi
			continuestate=2
			;&
		3)  formatdisks
			continuestate=3
			;&
		4)  turnonswap
			continuestate=4
			;&
		5)  setuproot
			continuestate=5
			;&
		6)  refreshmirrors || true
			continuestate=6
			;&
		7) installarchlinux
			continuestate=7
			;&
		8) ntpsynctime
			continuestate=8
			;&
		9) genlocales
			continuestate=9
			;&
		10) sethostname
			continuestate=10
			;&
		11) geninitramfs
			continuestate=11
			;&
		12) createnewuser
			continuestate=12
			;&
		13) sdenablebluetooth
			continuestate=13
			;&
		14) sdenablenetwork
			continuestate=14
			;&
		15) sdenabletlp
			continuestate=15
			;&
		16) setupgraphics
			continuestate=16
			;&
		17) if [[ $enablefingerprint = true ]];
			then
		 		setupfingerprint
			fi
			continuestate=17
			;&
		18) setuptouchpad
			continuestate=18
			;&
		19) installaurhelpers
			continuestate=19
			;&
		20) installaurpackages
			continuestate=20
			;&
		21) installleftwmtheme
			continuestate=21
			;&
		22) setupgtktheme
			continuestate=22
			;&
		23) setuplockscreen
			continuestate=23
			;&
		24) setuppicomconf
			continuestate=24
			;&
		25) setupsxhkdrc
			continuestate=25
			;&
		26) setupxinitrc
			continuestate=26
			;&
		27) setupfishstartx
			continuestate=27
			;&
		28) installbootloader
			continuestate=28
			;&
		29) installsecureboot
			continuestate=29
			;&
		30) changedefaultshell
			continuestate=30
			;&
		31) finishinstall
	esac
}


function updatescript(){
	curl -L "https://bit.ly/aim-arch-install-leftwm" -o $1 && exit 0
}


function mountall(){
    swapon $installon$ssdpartitionseperator$swappartition
    mount -o subvol=@,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt
    mount -o subvol=@home,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt/home
    mount -o subvol=@var,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt/var
    mount -o subvol=@snapshots,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt/.snapshots
    mount $installon$ssdpartitionseperator$esppartition /mnt/boot
}


function unmountall(){
	swapoff $installon$ssdpartitionseperator$swappartition || true
	umount /mnt/home || true
	umount /mnt/var || true
	umount /mnt/.snapshots || true
	umount /mnt || true
	umount $installon$ssdpartitionseperator$rootpartition || true
}


function archchroot(){
    arch-chroot /mnt /bin/bash
}


function updatesystime() {
	systemctl enable --now systemd-timesyncd
	timedatectl set-ntp true
}


function partitiondisks(){
	if [[ $removepartitions = true ]]
	then
		parted --script $installon \
		rm $esppartition \
		rm $swappartition \
		rm $rootpartition \
		quit
	fi
	parted --script $installon \
	mkpart primary fat32 $((espstartmib))MiB $((espendmib))MiB \
	mkpart primary linux-swap $((swapstartmib))MiB $((swapendmib))MiB \
	mkpart primary btrfs $((rootstartmib))MiB $((rootendmib))MiB \
	set $esppartition esp on \
	set $swappartition swap on \
	unit MiB print \
	quit
}


function formatdisks(){
	mkfs.fat -F32 $installon$ssdpartitionseperator$esppartition
	mkswap $installon$ssdpartitionseperator$swappartition
	mkfs.btrfs -L "Computer" -f $installon$ssdpartitionseperator$rootpartition
}


function turnonswap(){
	swapon $installon$ssdpartitionseperator$swappartition
}


function setuproot(){
	mount $installon$ssdpartitionseperator$rootpartition /mnt
	btrfs su cr /mnt/@
	btrfs su cr /mnt/@home
	btrfs su cr /mnt/@var
	btrfs su cr /mnt/@snapshots
	umount /mnt
	mount -o subvol=@,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt
	mkdir /mnt/{boot,home,var,.snapshots}
	mount -o subvol=@home,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt/home
	mount -o subvol=@var,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt/var
	mount -o subvol=@snapshots,compress=zstd,noatime,nodiratime $installon$ssdpartitionseperator$rootpartition /mnt/.snapshots
	mount $installon$ssdpartitionseperator$esppartition /mnt/boot
}


function refreshmirrors(){
	sed -i '/\[multilib\]/,/mirrorlist/ s/#//' /etc/pacman.conf
	pacman -Syy --noconfirm reflector
	reflector --verbose --sort rate --save /etc/pacman.d/mirrorlist
}


function installarchlinux(){
	pacstrap /mnt $pacmanpkg
	genfstab -U /mnt >> /mnt/etc/fstab
}


function runinchroot(){
	arch-chroot /mnt /bin/bash -c "$@"
}

function runasuser(){
	runinchroot "su $pcusername -c -- '$@'"
}


function ntpsynctime(){
	runinchroot "ln -sf /usr/share/zoneinfo/$timezone /etc/localtime && \
	hwclock --systohc && \
	systemctl enable systemd-timesyncd && \
	timedatectl set-ntp true"
}


function genlocales(){
	sed -i "/$language/s/^#//g" /mnt/etc/locale.gen
	runinchroot "locale-gen"
	echo "LANG=$language" >> /mnt/etc/locale.conf
}


function sethostname(){
	echo $pchostname >> /mnt/etc/hostname
}


function geninitramfs(){
	sed -i 's/block filesystems/block btrfs filesystems/' /mnt/etc/mkinitcpio.conf
	sed -i "/MODULES=()/s/()/($kernelmodules)/" /mnt/etc/mkinitcpio.conf
	runinchroot "mkinitcpio -P"
}


function createnewuser(){
	runinchroot "passwd -l root && \
	useradd -mg users -G wheel,storage,power -s /bin/bash $pcusername && \
	echo $pcusername:$pcpassword | chpasswd"
	sed -i '/%wheel ALL=(ALL) ALL/s/# //' /mnt/etc/sudoers
	runasuser "cd /home/$pcusername && mkdir Documents Downloads Music Pictures Videos" || true
}


function sdenablebluetooth(){
	runinchroot "systemctl enable bluetooth"
}


function sdenablenetwork(){
	runinchroot "systemctl enable dhcpcd"
	runinchroot "systemctl enable iwd"
}


function sdenabletlp(){
	runinchroot "tlp start || true"
}


function setupgraphics(){
	(
        echo 'Section "Device"'
        echo '    Identifier '\"$graphicsprovider\"
        echo '    Driver '\"$graphicsdriver\"
        echo '    Option "TearFree" "true"'
        echo '    Option "DRI" "3"'
        echo 'EndSection'
    ) > /mnt/etc/X11/xorg.conf.d/20-$graphicsdriver.conf
}

function setupfingerprint(){
	sed -i '3iauth            sufficient      pam_fprintd.so' /mnt/etc/pam.d/system-auth
}


function setuptouchpad(){
	(
		echo 'Section "InputClass"'
		echo '        Identifier "MyTouchapad"'
		echo '        Driver "libinput"'
		echo '        Option "Tapping" "on"'
		echo '        Option "NaturalScrolling" "True"'
		echo 'EndSection'
	) > /mnt/etc/X11/xorg.conf.d/30-touchpad.conf
}


function installaurhelpers(){
	if [[ -d /mnt/home/$pcusername/.cache/aurinstaller ]]
	then
		rm -r /mnt/home/$pcusername/.cache/aurinstaller
	fi
	runasuser "mkdir /home/$pcusername/.cache || true"
	runasuser "mkdir /home/$pcusername/.cache/aurinstaller"
	runasuser "git clone https://aur.archlinux.org/$aurwrapper.git /home/$pcusername/.cache/aurinstaller/$aurwrapper"
	sed -i '/%wheel ALL=(ALL) ALL/s/%/# %/g' /mnt/etc/sudoers
	sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/# //g' /mnt/etc/sudoers
	runasuser "export LANG=$language && cd /home/$pcusername/.cache/aurinstaller/$aurwrapper && makepkg --syncdeps -si --noconfirm || true"
	sed -i '/%wheel ALL=(ALL) ALL/s/# //g' /mnt/etc/sudoers
	sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/%/# %/g' /mnt/etc/sudoers
	rm -rf /mnt/home/$pcusername/.cache/aurinstaller || true
}


function aurinstall(){
	sed -i '/%wheel ALL=(ALL) ALL/s/%/# %/g' /mnt/etc/sudoers
	sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/# //g' /mnt/etc/sudoers
	runasuser "export LANG=$language && $aurwrapper -Syy --noconfirm $@ ||true"
	sed -i '/%wheel ALL=(ALL) ALL/s/# //g' /mnt/etc/sudoers
	sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/%/# %/g' /mnt/etc/sudoers
}


function installaurpackages(){
	aurinstall "$aurpkg"
}

function installleftwmtheme(){
	runasuser "leftwm-theme update && \
	leftwm-theme install Soothe && \
	leftwm-theme apply Soothe"
	sed -i 's/wlp3s0/wlan0/g' /mnt/home/$pcusername/.config/leftwm/themes/Soothe/theme/polybar.conf
}

function setupgtktheme(){
	runasuser "mkdir -p /home/$pcusername/.config/gtk-3.0"
	(
		echo '[Settings]'
		echo 'gtk-application-prefer-dark-theme=1'
		echo 'gtk-theme-name=Plata-Noir-Compact'
		echo 'gtk-icon-theme-name=Tela-black-dark'
		echo 'gtk-font-name=Cantarell 11'
		echo 'gtk-cursor-theme-name=Adwaita'
		echo 'gtk-cursor-theme-size=0'
		echo 'gtk-toolbar-style=GTK_TOOLBAR_BOTH'
		echo 'gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR'
		echo 'gtk-button-images=1'
		echo 'gtk-menu-images=1'
		echo 'gtk-enable-event-sounds=1'
		echo 'gtk-enable-input-feedback-sounds=1'
		echo 'gtk-xft-antialias=1'
		echo 'gtk-xft-hinting=1'
		echo 'gtk-xft-hintstyle=hintfull'
	) > /mnt/home/$pcusername/.config/gtk-3.0/settings.ini
	runinchroot "chown -R $pcusername /home/$pcusername"
}

function setuplockscreen(){
	sed -i 's/slock/i3lock-fancy-multimonitor/g' /mnt/home/$pcusername/.config/leftwm/themes/Soothe/leftwm/config.toml
	runinchroot "chown -R $pcusername /home/$pcusername"
}

function setuppicomconf(){
	runasuser "mkdir -p /home/$pcusername/.config/picom/"
	runasuser "cp /etc/xdg/picom.conf.example /home/$pcusername/.config/picom/picom.conf"
	sed -i 's/# blur\-method =/blur-method = "dual_kawase"/g' /mnt/home/$pcusername/.config/picom/picom.conf
	sed -i 's/# blur\-size = 12/blur\-size = 10/g' /mnt/home/$pcusername/.config/picom/picom.conf
	sed -i 's/backend = "xrender"/# backend = "xrender"/g' /mnt/home/$pcusername/.config/picom/picom.conf
	sed -i 's/# backend = "glx"/backend = "glx"/g' /mnt/home/$pcusername/.config/picom/picom.conf
	runinchroot "chown -R $pcusername /home/$pcusername"
}

function setupsxhkdrc(){
	runasuser "mkdir -p /home/$pcusername/.config/sxhkd"
	runasuser "touch /home/$pcusername/.config/sxhkd/sxhkdrc"
	(
		echo 'XF86Audio{Raise,Lower}Volume'
		echo '        amixer set Master 5%{+,-}'
		echo ''
		echo 'XF86MonBrightnessUp'
		echo '        brightnessctl set +10%'
		echo ''
		echo 'XF86MonBrightnessDown'
		echo '        brightnessctl set 10%-'
		echo ''
		echo 'XF86AudioMute'
		echo '        amixer set Master toggle'
	) >> /mnt/home/$pcusername/.config/sxhkd/sxhkdrc
	runinchroot "chown -R $pcusername /home/$pcusername"
}

function setupxinitrc(){
	runasuser "cp /etc/X11/xinit/xinitrc /home/$pcusername/.xinitrc"
	sed -i '/twm/d' /mnt/home/$pcusername/.xinitrc
	sed -i '/xterm/d' /mnt/home/$pcusername/.xinitrc
	sed -i '/xclock/d' /mnt/home/$pcusername/.xinitrc
	(
		echo 'sxhkd &'
		echo 'xidlehook \'
		echo '  --not-when-fullscreen \'
		echo '  --not-when-audio \'
		echo '  --timer 120 \'
		echo '    '"'"'brightnessctl --save; brightnessctl set 10%'"'"' \'
		echo '    '"'"'brightnessctl --restore'"'"' \'
		echo '  --timer 180 \'
		echo '    '"'"'i3lock-fancy-multimonitor'"'"' \'
		echo '    '"'"'brightnessctl --restore'"'"' \'
		echo '  --timer 30 \'
		echo '    '"'"'xset dpms force off'"'"' \'
		echo '    '"'"'brightnessctl --restore'"'"' \'
		echo '  --timer 870 \'
		echo '    '"'"'systemctl suspend'"'"' \'
		echo '    '"'"'brightnessctl --restore'"'"' \'
		echo '  &'
		echo 'exec dbus-launch leftwm'
	) >> /mnt/home/$pcusername/.xinitrc
	runinchroot "chown -R $pcusername /home/$pcusername"
}

function setupfishstartx(){
	runasuser "mkdir -p /home/$pcusername/.config/fish"
	runasuser "cp /etc/fish/config.fish /home/$pcusername/.config/fish/config.fish"
	(
		echo 'set fish_greeting'
		echo 'if status is-login'
		echo '    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1'
		echo '        exec startx -- -keeptty'
		echo '    end'
		echo 'end'
	) >> /mnt/home/$pcusername/.config/fish/config.fish
	runinchroot "chown -R $pcusername /home/$pcusername"
}


function installbootloader(){
	runinchroot "bootctl --path=/boot install"

	(
        echo "default  arch"
        echo "timeout  0"
        echo "console-mode   max"
        echo "editor   0"
    ) > /mnt/boot/loader/loader.conf

    (
        echo "title Arch Linux"
        echo "linux /vmlinuz-linux"
        echo "initrd /$microcodepkg.img"
        echo "initrd /initramfs-linux.img"
        echo "options $kerneldef $kernelargs"
    ) > /mnt/boot/loader/entries/arch.conf
}


function installsecureboot(){
	aurinstall preloader-signed
	yes | cp -f /mnt/usr/share/preloader-signed/HashTool.efi /mnt/boot/EFI/Boot
	yes | cp -f /mnt/boot/EFI/systemd/systemd-bootx64.efi /mnt/boot/EFI/Boot/loader.efi
	yes | cp -f /mnt/usr/share/preloader-signed/PreLoader.efi /mnt/boot/EFI/Boot/bootx64.efi
}

function changedefaultshell(){
	runinchroot "chsh -s /usr/bin/fish aim && \
	chsh -s /usr/bin/fish"
}


function finishinstall(){
	umount -a || true
	systemctl reboot
}


main "$@"
