# Arch Install

Fully automatic Arch install script without the need of human intervention. Edit script before running. Fork the script and edit the parameters it to make it truly your own.

## Warning!

- Do not use the script without any modification to it as you might end up accidentally destroying all your data.
- Read the script completely and use it after modifying it to your preference.

## Instructions:

- Fork it to your repo and modify it to your preference
- Use url shortners to use it along with curl.

## Install Using:

```bash
curl -L "https://bit.ly/aim-arch-install" -o ./install.sh
chmod +x ./install.sh
./install.sh
```

for a minimal install, use:

```bash
curl -L "https://bit.ly/aim-arch-install-min" -o ./install-min.sh
chmod +x ./install-min.sh
./install-min.sh
```

for a leftwm install, use:

```bash
curl -L "https://bit.ly/aim-arch-install-leftwm" -o ./install-leftwm.sh
chmod +x ./install-leftwm.sh
./install-leftwm.sh
```

for a xmonad install, use:

```bash
curl -L "https://bit.ly/aim-arch-install-xmonad" -o ./install-xmonad.sh
chmod +x ./install-xmonad.sh
./install-xmonad.sh
```

### for running help use:

```bash
./install.sh -h
```

for minimal installer, use:

```bash
./install-min.sh -h
```

for a leftwm install, use:

```bash
./install-leftwm.sh -h
```

for a xmonad install, use:

```bash
./install-xmonad.sh -h
```
